# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Golang
      module Build
        def self.main
          System.run('apk add --no-cache go')
          System.run('go version')
        end
      end

    end
  end
end
